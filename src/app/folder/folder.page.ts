import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { Platform, NavController, ToastController, LoadingController } from "@ionic/angular";
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  LatLng,
  MarkerOptions,
  Marker,
  GoogleMapsAnimation,
  MyLocation
} from "@ionic-native/google-maps";

@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements AfterViewInit {

  map: GoogleMap;
  loading: any;

  constructor(public googleMaps: GoogleMaps,
    public plt: Platform,
    public nav: NavController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController) {
    console.log('Construtor');
  }

  ngAfterViewInit() {
    this.plt.ready().then(() => {
      this.initMap();
    });
  }


  initMap() {
    console.log('InitMap Iniciando');
    this.map = this.googleMaps.create('map');
    this.map.one(GoogleMapsEvent.MAP_READY).then((data: any) => {
      let coordinates: LatLng =
        new LatLng(-2.549531, -44.240750);
      let position = {
        target: coordinates,
        zoom: 17
      };
      this.map.animateCamera(position);
      let markerOptions: MarkerOptions = {
        position: coordinates,
        icon: "asserts/images/icons8-Marker-64.png",
        title: 'CEST',
        snippet: 'http://www.cest.edu.br/',
        animation: GoogleMapsAnimation.DROP
      };

      const marker = this.map.addMarker(markerOptions).
        then((marker: Marker) => {
          marker.showInfoWindow();
        });
    })
  }

  async onGPSClick() {
    this.map.clear();

    this.loading = await this.loadingCtrl.create({
      message: 'Aguarde...'
    });

    await this.loading.present();

    this.map.getMyLocation().then((location: MyLocation) => {
      this.loading.dismiss();
      console.log(JSON.stringify(location, null, 2));

      this.map.animateCamera({
        target: location.latLng,
        zoom: 17,
        tilt: 38
      });

      let marker: Marker = this.map.addMarkerSync({
        title: 'Eu estou aqui',
        snippet: 'Um subtítulo',
        position: location.latLng,
        animation: GoogleMapsAnimation.BOUNCE
      });

      marker.showInfoWindow();

      marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
        this.showToast('clicou!');
      });
    }).catch(err => {
      this.loading.dismiss();
      this.showToast(err.error_message);
    });
  }

  async showToast(message: string) {
    let toast = await this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'middle'
    });

    toast.present();
  }

}
